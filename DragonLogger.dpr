program DragonLogger;

uses
  Windows,
  Forms,
  SysUtils,
  Vcl.Dialogs,
  UFrmMain in 'Src\UFrmMain.pas' {FrmDBrowser},
  UDlgInventory in 'Src\UDlgInventory.pas' {DlgInventory},
  UStrings in 'Src\UStrings.pas',
  UConfig in 'Src\UConfig.pas',
  UDataConfig in 'Src\UDataConfig.pas',
  UZoneConfig in 'Src\UZoneConfig.pas',
  UDlgPageSource in 'Src\UDlgPageSource.pas' {DlgPageSource},
  UDlgAbout in 'Src\UDlgAbout.pas' {DlgAbout},
  UDlgHotKeys in 'Src\UDlgHotKeys.pas' {DlgHotkeys},
  UDlgPreferences in 'Src\UDlgPreferences.pas' {DlgPreferences},
  UDlgAnalyze in 'Src\UDlgAnalyze.pas' {Form1},
  UDlgCubeProgress in 'Src\UDlgCubeProgress.pas' {DlgCubeProgress},
  UParser in 'Src\UParser.pas';

var
  Mutex: THandle;

{$R *.res}

begin
  // Mutex to only allow a single instance of the application
  Mutex := CreateMutex(nil, True, 'DragonTavernLogger');
  if (Mutex <> 0) and (GetLastError = 0) then begin
    try
      Application.Initialize;
      Application.MainFormOnTaskbar := True;
      Application.CreateForm(TFrmDBrowser, FrmDBrowser);
  Application.CreateForm(TDlgCubeProgress, DlgCubeProgress);
  Application.Run;
    finally
      if Mutex <> 0 then
        CloseHandle(Mutex);
    end;
  end else
    ShowMessage(Format(strAppAlreadyRunning, [strAppName]));
end.
