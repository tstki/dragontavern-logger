unit UParser;

interface

uses
  UDlgInventory,
  Classes;

type
  TParser = class(TObject)
  private
    { Private declarations }
    //dataIni: TIniFile;
  public
    { Public declarations }
    procedure Initialize();
    function DataExtraction(const Html: String; LogEventCreatures, DetectSoulStatus: boolean; var CharName, ZoneName, CreatureType: String; var ShowSoulsWarning: Boolean): Boolean;
    function GatherItems(const Html: String; InvItems: TInvItemList; var CubeCurCharges, CubeMaxCharges: Integer): Boolean;
  end;

implementation

uses
  System.StrUtils,
  SysUtils, Forms, UStrings;

//-----------------------------------------------------------------------------
// Name: cnsSmartPos
// Author: Com-N-Sense
// Source: http://www.swissdelphicenter.ch/torry/showcode.php?id=2125
// Date:
// Purpose: Find a substring in a string starting from any position in the string.
// Params: SubStr - a substring for search.
//         S - the source string to search within
//         StartPos - the index position to start the search.
// Result: Integer - the position of the substring,
//                   zero - if the substring was not found
// Remarks: This is the original Delphi "Pos" function modified to support
//          the start pos parameter.
//-----------------------------------------------------------------------------
function SmartPosAsm(const substr: AnsiString; const s: AnsiString; StartPos: Cardinal) : Integer;
type
  StrRec = packed record
    allocSiz: Longint;
    refCnt: Longint;
    length: Longint;
  end;
const
  skew = sizeof(StrRec);
asm // Assembly
{ ->EAX     Pointer to substr               }
{   EDX     Pointer to string               }
{ <-EAX     Position of substr in s or 0    }
    TEST    EAX,EAX
    JE      @@noWork

    TEST    EDX,EDX
    JE      @@stringEmpty

    PUSH    EBX
    PUSH    ESI
    PUSH    EDI

    MOV     ESI,EAX                         { Point ESI to substr           }
    MOV     EDI,EDX                         { Point EDI to s                }

    MOV     EAX,ECX
    MOV     ECX,[EDI-skew].StrRec.length    { ECX = Length(s)               }
    ADD     EDI,EAX
    SUB     ECX,EAX

    PUSH    EDI                             { remember s position to calculate index        }

    MOV     EDX,[ESI-skew].StrRec.length    { EDX = Length(substr)          }

    DEC     EDX                             { EDX = Length(substr) - 1              }
    JS      @@fail                          { < 0 ? return 0                        }
    MOV     AL,[ESI]                        { AL = first char of substr             }
    INC     ESI                             { Point ESI to 2'nd char of substr      }

    SUB     ECX,EDX                         { #positions in s to look at    }
                                            { = Length(s) - Length(substr) + 1      }
    JLE     @@fail
@@loop:
    REPNE   SCASB
    JNE     @@fail
    MOV     EBX,ECX                         { save outer loop counter               }
    PUSH    ESI                             { save outer loop substr pointer        }
    PUSH    EDI                             { save outer loop s pointer             }

    MOV     ECX,EDX
    REPE    CMPSB
    POP     EDI                             { restore outer loop s pointer  }
    POP     ESI                             { restore outer loop substr pointer     }
    JE      @@found
    MOV     ECX,EBX                         { restore outer loop counter    }
    JMP     @@loop

@@fail:
    POP     EDX                             { get rid of saved s pointer    }
    XOR     EAX,EAX
    JMP     @@exit

@@stringEmpty:
    XOR     EAX,EAX
    JMP     @@noWork

@@found:
    POP     EDX                             { restore pointer to first char of s    }
    MOV     EAX,EDI                         { EDI points of char after match        }
    SUB     EAX,EDX                         { the difference is the correct index   }
@@exit:
    POP     EDI
    POP     ESI
    POP     EBX
@@noWork:
end; //SmartPosAsm

function cnsSmartPos(const substr: AnsiString; const s: AnsiString; StartPos: Cardinal): Integer;
begin
  dec(StartPos);
  Result := SmartPosAsm(SubStr, S, StartPos);
  if Result > 0 then
    Result := Result + Integer(StartPos);
end; //cnsSmartPos

procedure TParser.Initialize();
begin
//  dataIni := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
end;

function TParser.DataExtraction(const Html: String; LogEventCreatures, DetectSoulStatus: boolean; var CharName, ZoneName, CreatureType: String; var ShowSoulsWarning: Boolean): Boolean;
var
  p1, p2, intSize: Integer;
  tmpstr: String;
begin
  Result := False;

  p1 := Pos(PChar(cExploreStr), Html);
  if p1 <> 0 then begin

    p2 := Pos(PChar('</SPAN></DIV>'), Html);
    if p2 <> 0 then begin
      tmpstr := Copy(Html, p1+Length(cExploreStr), p2-p1-Length(cExploreStr));
      p1 := Pos(PChar('/">'), tmpstr);
      CharName := Copy(tmpstr, 1, p1-1);

      p2 := Pos(PChar('</A><SPAN>'), tmpstr);
      ZoneName := Trim(Copy(tmpstr, p1+Length(PChar('/">')), p2-p1-Length(PChar('/">'))));

      // See if we found a creature. (We don't care about traps, nothing, and dead corpses recovered)
      p1 := Pos(PChar('<H4>Level '), Html);
      if p1 <> 0 then begin

        p2 := Pos(PChar('</H4>'), Html);
        if p2 <> 0 then begin
          tmpstr := Copy(Html, p1+Length(PChar('<H4>Level ')), p2-p1-Length(PChar('<H4>Level ')));

          if StrToIntDef(Copy(tmpstr, 1, 3), -1) <> -1 then
            intSize := 3
          else if StrToIntDef(Copy(tmpstr, 1, 2), -1) <> -1 then
            intSize := 2
          else if StrToIntDef(Copy(tmpstr, 1, 1), -1) <> -1 then
            intSize := 1
          else
            intSize := 0;

          if intSize > 0 then begin

            // Check if this is a double XP (festivus) monster, and whether we want to log these
            p1 := Pos(PChar('<SPAN class=gold>Double XP'), Html);
            if (p1 = 0) or LogEventCreatures then begin
              creatureType := Copy(tmpstr, 1+intSize+1, Length(tmpstr)-intSize-1);

              // Check for boss type
              p1 := Pos(PChar(' <SPAN class='), creatureType);
              if p1 <> 0 then
                creatureType := Copy(creatureType, 1, p1-1);

              // We found a creature, inform the caller we got a result
              Result := True;

              // See if the user is immortal, and just harvested a soul
              if DetectSoulStatus then begin
                p1 := Pos(PChar(cSoulAdded), Html);
                if p1 <> 0 then begin
                  p2 := Pos(PChar(cSoulBursting), Html);
                  ShowSoulsWarning := p2 <> 0;
                end;
              end
            end;
          end;
        end;
      end;
    end;
  end;
end;


function TParser.GatherItems(const Html: String; InvItems: TInvItemList; var CubeCurCharges, CubeMaxCharges: Integer): Boolean;
var
  p1, p2, p3, p4, RarityVal: Integer;
  tmpstr, itemName, itemRarity, itemWeight, itemValue, itemRecycleURL: String;
  Outerhtml: String;
begin

//uses
//  RegularExpressions, // For TRegEx
//  Matches: TMatchCollection;
//  Matches := TRegex.Create('<meta.*?content=.*?url=([^"]*)"').Matches(Result);
//  if Matches.Count > 0 then begin
//    Url := Matches.Item[0].Groups[1].Value;
//    Result := FGetURLSourceAsString(Url, Depth+1);

  p1 := Pos(PChar(cInventoryStr), Html);
  if p1 <> 0 then begin
    p2 := Pos(PChar('Total Value:'), Html);
    if p2 <> 0 then begin
      // Inventory block found
      tmpstr := Copy(Outerhtml, p1+Length(cInventoryStr), p2-p1-Length(cInventoryStr));

      // Find all items:
      repeat
        itemName := '';
        itemRarity := '';
        itemValue := '';
        itemRecycleURL := '';

        // Find the item Name
        p1 := Pos(PChar(cInvItemNameStart), tmpstr);
        p2 := Pos(PChar(cBoldEnd), tmpstr);
        if (p1 <> 0) and (p2 <> 0) then begin
          itemName := Copy(tmpstr, p1+Length(cInvItemNameStart), p2-p1-Length(cInvItemNameStart));

          // During special events some items can be colored differently. Clean them up:
          itemName := StringReplace(itemName, '<SPAN class=orange>', '', [rfReplaceAll]);
          itemName := StringReplace(itemName, '</SPAN>', '', [rfReplaceAll]);

          // Find the Rarity
          p1 := Pos(PChar(cInvLootTypeStart), tmpstr);
          p2 := Pos(PChar(cInvLootTypeEnd), tmpstr);
          if (p1 <> 0) and (p2 <> 0) then begin
            itemRarity := Copy(tmpstr, p1+Length(cInvLootTypeStart), p2-p1-Length(cInvLootTypeStart));

            tmpstr := Copy(tmpstr, p2+Length(cInvLootTypeEnd), Length(tmpstr)-p2-Length(cInvLootTypeEnd));

            // Find the Gold Value
            p1 := Pos(PChar(cInvLootValueStart), tmpstr);
            p2 := Pos(PChar(cInvLootValueEnd), tmpstr);
            if (p1 <> 0) and (p2 <> 0) then begin
              itemValue := Copy(tmpstr, p1+Length(cInvLootValueStart), p2-p1-Length(cInvLootValueStart));

              tmpstr := Copy(tmpstr, p2+Length(cInvLootValueEnd), Length(tmpstr)-p2-Length(cInvLootValueEnd));

              // Find the item Weight
              p1 := Pos(PChar(cInvLootWeightStart), tmpstr);
              p2 := Pos(PChar(cSpanEnd), tmpstr);
              if (p1 <> 0) and (p2 <> 0) then begin
                itemWeight := Copy(tmpstr, p1+Length(cInvLootWeightStart), p2-p1-Length(cInvLootWeightStart));

                // Find the cube recycle url
                p3 := cnsSmartPos(AnsiString(cInvRecycleStart), AnsiString(tmpstr), 0);
                p4 := cnsSmartPos(AnsiString('"'), AnsiString(tmpstr), p3+Length(cInvRecycleStart));
                if (p3 <> 0) and (p4 <> 0) then begin
                  itemRecycleURL := Copy(tmpstr, p3+Length(cInvRecycleStart), p4-p3-Length(cInvRecycleStart));
                end;

                if itemRarity = 'com>[C' then
                  RarityVal := 1
                else if itemRarity = 'rar>[R' then
                  RarityVal := 2
                else if itemRarity = 'exo>[E' then
                  RarityVal := 3
                else
                  RarityVal := 0;

                itemValue := AnsiReplaceStr(itemValue, ',', '');

                InvItems.AddItem(itemRecycleURL, itemName, RarityVal, StrToIntDef(itemValue, 1), StrtoIntDef(itemWeight, 1));
              end;
            end; // Gold value
          end; // Rarity
        end; // Item name
      until itemName = '';
    end;
  end;

  // Determine Loot transmutation cube values

  CubeCurCharges := 0;
  CubeMaxCharges := 0;

  // Lets see if the user has a cube.
  p1 := Pos(PChar(cCubeStartStr), Outerhtml);
  if p1 <> 0 then begin
    p2 := Pos(PChar(cCubeEndStr), Outerhtml);
    if p2 <> 0 then begin
      tmpstr := Copy(Outerhtml, p1+Length(cCubeStartStr), p2-p1-Length(cCubeStartStr));
      //'</SPAN> has <SPAN style="COLOR: white">40</SPAN> charges (out of <SPAN class=white>40</SPAN>) '

      p1 := cnsSmartPos(AnsiString('">'), AnsiString(tmpstr), 0);
      if p1 <> 0 then begin
        p2 := cnsSmartPos(AnsiString('</SPAN>'), AnsiString(tmpstr), p1);
        if p2 <> 0 then begin
          // Current charges left, found!
          CubeCurCharges := StrToIntDef(Copy(tmpstr, p1+2, p2-p1-2), 0);

          p1 := cnsSmartPos(AnsiString('white>'), AnsiString(tmpstr), p2);
          if p1 <> 0 then begin
            p1 := cnsSmartPos(AnsiString('>'), AnsiString(tmpstr), p1);
            if p1 <> 0 then begin
              p2 := cnsSmartPos(AnsiString('<'), AnsiString(tmpstr), p1);
              // Max charges found as well!
              if p2 <> 0 then
                CubeMaxCharges := StrToIntDef(Copy(tmpstr, p1+1, p2-p1-1), 0);
            end;
          end;
        end;
      end;
    end;
  end;

  Result := InvItems.Count > 0;
end;

end.
